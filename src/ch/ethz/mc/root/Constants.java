package ch.ethz.mc.root;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletContext;

import lombok.Cleanup;
import lombok.Getter;
import lombok.Synchronized;
import lombok.val;
import lombok.extern.log4j.Log4j2;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Andreas Filler
 */
@Log4j2
public class Constants {
	private static boolean			injectionPerformed				= false;

	// Servlet-internal configuration
	public static final String		SYSTEM_CONFIGURATION_PROPERTY	= "mc_website.configuration";
	public static final String		LOGGING_APPLICATION_NAME		= "MC_WEBSITE";

	public static final String		WWW_FOLDER						= "www";
	public static final String		ROOT_REDIRECT					= WWW_FOLDER
																			+ "/root";

	// Website configuration
	@Getter
	private static String			websiteDataContentPath			= "/mc_data/website/content";
	@Getter
	private static String			websiteDataFilesPath			= "/mc_data/website/files";

	// defaultURL must start with www.
	@Getter
	private static String			defaultURL						= "https://www.your_mc_website.eu";
	// websiteDataURL must start with data.
	@Getter
	private static String			websiteDataURL					= "https://data.your_mc_website.eu";

	// Statistics configuration
	@Getter
	private static String			statisticsFile					= "/mc_data/statistics.properties";

	// Individual MobileCoach instance specific configuration

	// The constant #PATH#file# can be used instead of a folder names for
	// feedback, files, etc. specific redirects
	@Getter
	private static String			configuratedSubDomains			= "w:#/MC/surveys-short#file#,f:#/MC/files-short#file#,test:example";

	// Current version information
	private static String			version							= null;
	private static ServletContext	servletContext					= null;

	/**
	 * Enables the application to retrieve the current version information
	 *
	 * @return Current version
	 */
	@Synchronized
	public static String getVersion() {
		try {

			if (version == null) {
				@Cleanup
				val inputStream = servletContext
						.getResourceAsStream("/WEB-INF/version.txt");
				@Cleanup
				val inputStreamReader = new InputStreamReader(inputStream);
				@Cleanup
				val bufferedInputStreamReader = new BufferedReader(
						inputStreamReader);

				version = bufferedInputStreamReader.readLine();
			}
		} catch (final Exception e) {
			log.error("Error at parsing version file: {}", e.getMessage());
			return "---";
		}
		return version;
	}

	/**
	 * Injects a specific configuration file (if provided as system parameter
	 * "mc_website.configuration")
	 *
	 * @param servletContext
	 *
	 * @param configurationsFile
	 */
	public static void injectConfiguration(
			final String configurationsFileString,
			final ServletContext servletContext) {
		// Ensure that this method can only be called once
		if (!injectionPerformed) {
			injectionPerformed = true;

			Constants.servletContext = servletContext;

			if (configurationsFileString == null) {
				return;
			}

			val configurationFile = new File(configurationsFileString);

			if (!configurationFile.exists()) {
				log.error("Provided configuration file {} does not exist",
						configurationFile.getAbsoluteFile());
				return;
			}

			// Configuration file provided and exists
			log.info("Reading from configuration file {}",
					configurationFile.getAbsoluteFile());

			val properties = new Properties();
			try {
				@Cleanup
				val fileInputStream = new FileInputStream(configurationFile);
				properties.load(fileInputStream);
			} catch (final Exception e) {
				log.error("Error at parsing provided configuration file: {}",
						e.getMessage());
				return;
			}

			// Check all parameters in Constants if it's provided in the
			// configuration file to overwrite the standard value
			for (val field : Constants.class.getDeclaredFields()) {
				if (field != Constants.class.getDeclaredFields()[0]) {
					val fieldName = field.getName();
					val fieldType = field.getType();

					if (properties.containsKey(fieldName)) {
						log.debug("Overwriting '{}' with value '{}'",
								fieldName, properties.getProperty(fieldName));

						try {
							if (fieldType == String.class) {
								field.set(null,
										properties.getProperty(fieldName));
							} else if (fieldType == Boolean.TYPE) {
								field.set(null, Boolean.parseBoolean(properties
										.getProperty(fieldName)));
							} else if (fieldType == Integer.TYPE) {
								field.set(null, Integer.parseInt(properties
										.getProperty(fieldName)));
							} else if (fieldType == Locale.class) {
								field.set(
										null,
										new Locale(properties
												.getProperty(fieldName)));
							} else {
								log.error(
										"Field '{}' seems to be of an unsupported type {}!",
										fieldName, fieldType);
							}
						} catch (final Exception e) {
							log.error(
									"Error at overwriting constants value: {}",
									e.getMessage());
						}

					}
				}
			}
		}
	}
}
