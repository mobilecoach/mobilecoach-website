package ch.ethz.mc.root;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.util.HashMap;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Synchronized;
import lombok.val;
import lombok.extern.log4j.Log4j2;

/**
 * Manages redirects to "www"-folder
 * 
 * @author Andreas Filler
 */
@Log4j2
public class RedirectManager {
	private static boolean		initialized	= false;

	@Getter
	private static final String	defaultURL	= Constants.getDefaultURL();

	private enum FixSubDomains {
		WWW("root"), DEV("dev"), DATA("#data#");

		@Getter(value = AccessLevel.PRIVATE)
		private final String	redirectValue;

		FixSubDomains(final String redirectValue) {
			this.redirectValue = redirectValue;
		}

		public String toSubDomain() {
			return toString().toLowerCase();
		}
	}

	private static HashMap<String, String>	configuratedSubDomains;

	/**
	 * Initialize list of allowed and configured sub domains
	 */
	@Synchronized
	public static void init() {
		// Ensure that this method can only be called once
		if (!initialized) {
			initialized = true;

			try {
				initialize();
			} catch (final Exception e) {
				log.error(
						"Error when parsing subdomains in configuration file: {}",
						e.getMessage());
			}
		}
	}

	private static void initialize() {
		configuratedSubDomains = new HashMap<String, String>();

		val newSubDomains = Constants.getConfiguratedSubDomains().split(",");

		for (val newSubDomain : newSubDomains) {
			val newSubDomainParts = newSubDomain.split(":");
			configuratedSubDomains.put(newSubDomainParts[0].toLowerCase(),
					newSubDomainParts[1]);
		}

		log.debug("Manually configured subdomains: {}", configuratedSubDomains);
	}

	public static String getAppropriateRedirectPath(final String serverName) {
		for (val subDomain : FixSubDomains.values()) {
			if (serverName.toLowerCase().startsWith(
					subDomain.toSubDomain() + ".")) {
				return Constants.WWW_FOLDER + "/"
						+ subDomain.getRedirectValue();
			}
		}
		for (val subDomain : configuratedSubDomains.keySet()) {
			if (serverName.toLowerCase().startsWith(subDomain + ".")) {
				return Constants.WWW_FOLDER + "/"
						+ configuratedSubDomains.get(subDomain);
			}
		}

		return null;
	}
}
