package ch.ethz.mc.root;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import lombok.Cleanup;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.balusc.webapp.FileServletWrapper;

import org.apache.commons.io.IOUtils;

/**
 * Servlet to map all files to the appropriate folders, based on the subdomain
 *
 * @author Andreas Filler
 */
@SuppressWarnings("serial")
@WebServlet(displayName = "Default", value = "/*", asyncSupported = true, loadOnStartup = 1)
@Log4j2
public class DefaultWrapperServlet extends HttpServlet {
	private FileServletWrapper	fileServletWrapper;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	@Override
	public void init(final ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		log.info("Initializing servlet...");

		fileServletWrapper = new FileServletWrapper();
		fileServletWrapper.init(getServletContext());

		log.info("Servlet initialized.");
	}

	/**
	 * Create the appropriate request object
	 *
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private HttpServletRequest createWrappedReqest(
			final HttpServletRequest request, final HttpServletResponse response)
			throws IOException, ServletException {
		final String basePath = RedirectManager
				.getAppropriateRedirectPath(request.getServerName());

		// No base path found so it's an unknown subdomain and we redirect to
		// the base URL
		if (basePath == null) {
			final String queryString = request.getQueryString() == null ? ""
					: "?" + request.getQueryString();

			log.debug("Redirecting to '{}'", RedirectManager.getDefaultURL()
					+ request.getRequestURI() + queryString);

			response.setStatus(301);
			response.setContentType("text/html");
			response.setHeader("Location", RedirectManager.getDefaultURL()
					+ request.getRequestURI() + queryString);
			return null;
		}

		// Special case: Redirect to feedback, file, etc. stream
		if (basePath.endsWith("#file#")) {
			final String queryString = request.getQueryString() == null ? ""
					: "?" + request.getQueryString();

			val splittedBasePath = basePath.split("#");
			val redirectPath = splittedBasePath[splittedBasePath.length - 2];

			log.debug(
					"Redirecting to feedback/file/... stream '{}'",
					RedirectManager.getDefaultURL() + redirectPath
							+ request.getRequestURI() + queryString);

			response.setStatus(301);
			response.setContentType("text/html");
			response.setHeader("Location", RedirectManager.getDefaultURL()
					+ redirectPath + request.getRequestURI() + queryString);
			return null;
		}

		// Special case: Redirect to website data stream
		if (basePath.endsWith("#data#")) {
			log.debug("Redirecting to website data stream '{}'",
					request.getRequestURI());

			val dataFolder = new File(Constants.getWebsiteDataFilesPath());
			if (!dataFolder.isDirectory() || !dataFolder.exists()) {
				response.setStatus(404);

				return null;
			}

			val file = new File(dataFolder, request.getRequestURI());

			if (!file.isFile() || !file.exists()) {
				response.setStatus(404);

				return null;
			}

			// Special case for videos: Streaming
			if (getServletContext().getMimeType(file.getName()).startsWith(
					"video/")) {
				val fileToServe = file;
				// Wrapping request
				final HttpServletRequest wrapped = new HttpServletRequestWrapper(
						request) {
					@Override
					public String getPathInfo() {
						return fileToServe.getAbsolutePath();
					}
				};

				return wrapped;
			} else {
				try {
					@Cleanup
					val inputStream = new BufferedInputStream(
							new FileInputStream(file));
					val mimeType = URLConnection
							.guessContentTypeFromStream(inputStream);

					response.setContentType(mimeType);
					IOUtils.copy(inputStream, response.getOutputStream());
					response.flushBuffer();
				} catch (final Exception e) {
					response.setStatus(500);

					return null;
				}
			}

			return null;
		}

		// Special case: Website presenter servlet
		if (basePath.equals(Constants.ROOT_REDIRECT)
				&& request.getRequestURI() != null
				&& request.getRequestURI().equals("/")) {
			log.debug("Redirecting to root website");

			final RequestDispatcher requestDispatcher = getServletContext()
					.getNamedDispatcher("Website-Presenter");
			requestDispatcher.forward(request, response);

			return null;
		}

		// Special case: Website admin servlet
		if (basePath.equals(Constants.ROOT_REDIRECT)
				&& request.getRequestURI() != null
				&& request.getRequestURI().equals("/admin")) {
			log.debug("Redirecting to root website admin");

			final RequestDispatcher requestDispatcher = getServletContext()
					.getNamedDispatcher("Website-Admin");
			requestDispatcher.forward(request, response);

			return null;
		}

		// Internal servlet redirects
		final String requestURI;
		final String pathInfo;
		final String pathTranslated;
		if (request.getRequestURI().endsWith("/")) {
			requestURI = request.getRequestURI() + "index.htm";
			pathInfo = request.getPathInfo() + "index.htm";
			pathTranslated = request.getPathTranslated() + "index.htm";
		} else {
			requestURI = request.getRequestURI();
			pathInfo = request.getPathInfo();
			pathTranslated = request.getPathTranslated();
		}

		log.debug(
				"Redirecting INTERNALLY to servlet base path '{}' with requestURI '{}', pathInfo '{}' and pathTranslated '{}'",
				basePath, requestURI, pathInfo, pathTranslated);
		final RequestDispatcher requestDispatcher = getServletContext()
				.getNamedDispatcher("default");

		final HttpServletRequest wrapped = new HttpServletRequestWrapper(
				request) {
			@Override
			public String getServletPath() {
				return "/" + basePath;
			}

			@Override
			public String getRequestURI() {
				return requestURI;
			}

			@Override
			public String getPathInfo() {
				return pathInfo;
			}

			@Override
			public String getPathTranslated() {
				return pathTranslated;
			}
		};

		requestDispatcher.forward(wrapped, response);
		return null;
	}

	/**
	 * Process HEAD request. This returns the same headers as GET request, but
	 * without content.
	 *
	 * @see HttpServlet#doHead(HttpServletRequest, HttpServletResponse).
	 */
	@Override
	protected void doHead(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		log.debug("Serving dynamic {}", request.getPathInfo());

		val wrapped = createWrappedReqest(request, response);

		if (wrapped != null) {
			fileServletWrapper.doHead(wrapped, response);
		}
	}

	/**
	 * Process GET request.
	 *
	 * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse).
	 */
	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		log.debug("Serving dynamic {}", request.getPathInfo());

		val wrapped = createWrappedReqest(request, response);

		if (wrapped != null) {
			fileServletWrapper.doGet(wrapped, response);
		}
	}

	/**
	 * Process POST request.
	 *
	 * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse).
	 */
	@Override
	protected void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		log.debug("Serving dynamic {}", request.getPathInfo());

		val wrapped = createWrappedReqest(request, response);

		if (wrapped != null) {
			fileServletWrapper.doGet(wrapped, response);
		}
	}
}
