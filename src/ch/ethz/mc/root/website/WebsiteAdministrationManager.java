package ch.ethz.mc.root.website;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import lombok.Cleanup;
import lombok.val;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringEscapeUtils;

import ch.ethz.mc.root.Constants;

/**
 * Manages Website Administration
 * 
 * @author Andreas Filler
 */
@Log4j2
public class WebsiteAdministrationManager {
	final HttpServletRequest	request;
	private final File			tempFolder;
	private final File			contentDataFolder;
	private final File			filesDataFolder;
	private final int			maxFileSize	= 100000 * 1024;
	private final int			maxMemSize	= 5000 * 1024;

	public WebsiteAdministrationManager(final HttpServletRequest request)
			throws Exception {
		this.request = request;

		contentDataFolder = new File(Constants.getWebsiteDataContentPath());
		if (!contentDataFolder.isDirectory() || !contentDataFolder.exists()) {
			throw new Exception("Content data folder \""
					+ Constants.getWebsiteDataContentPath()
					+ "\" does not exist!");
		}

		filesDataFolder = new File(Constants.getWebsiteDataFilesPath());
		if (!filesDataFolder.isDirectory() || !filesDataFolder.exists()) {
			throw new Exception("Files data folder \""
					+ Constants.getWebsiteDataFilesPath()
					+ "\" does not exist!");
		}

		tempFolder = new File(System.getProperty("java.io.tmpdir"));
		log.debug("Using temporary folder: {}", tempFolder.getAbsolutePath());
	}

	public boolean isAction(final String action) {
		if (request.getParameter("action") != null
				&& request.getParameter("action").equals(action)) {
			log.debug("It's action '{}'", action);
			return true;
		} else {
			return false;
		}
	}

	public void proceedUpload() {
		// Verify the content type
		final String contentType = request.getContentType();
		if (contentType != null
				&& contentType.indexOf("multipart/form-data") >= 0) {
			final DiskFileItemFactory factory = new DiskFileItemFactory();
			// maximum size that will be stored in memory
			factory.setSizeThreshold(maxMemSize);
			// Location to save data that is larger than maxMemSize.
			factory.setRepository(tempFolder);

			// Create a new file upload handler
			final ServletFileUpload upload = new ServletFileUpload(factory);
			// maximum file size to be uploaded.
			upload.setSizeMax(maxFileSize);
			try {
				// Parse the request to get file items.
				final List<FileItem> fileItems = upload.parseRequest(request);

				// Process the uploaded file items
				final Iterator<FileItem> i = fileItems.iterator();

				while (i.hasNext()) {
					final FileItem fi = i.next();
					File file;
					if (!fi.isFormField()) {
						// Get the uploaded file parameters
						// final String fieldName = fi.getFieldName();
						final String fileName = fi.getName();
						// final boolean isInMemory = fi.isInMemory();
						// final long sizeInBytes = fi.getSize();
						// Write the file
						if (fileName.lastIndexOf("/") >= 0) {
							file = new File(filesDataFolder, fileName
									.substring(fileName.lastIndexOf("/"))
									.replaceAll("[^A-Za-z0-9_.]+", "_"));
						} else {
							file = new File(filesDataFolder, fileName
									.substring(fileName.lastIndexOf("/") + 1)
									.replaceAll("[^A-Za-z0-9_.]+", "_"));
						}
						fi.write(file);
						log.debug("Uploaded file {}", file.getAbsoluteFile());
					}
				}
			} catch (final Exception e) {
				error("Error when uploading file: " + e.getMessage());
			}
		}
	}

	public void deleteFile(final String name) {
		log.debug(name);
		final File fileToDelete = new File(filesDataFolder, name);
		log.debug(fileToDelete.getAbsolutePath());
		if (fileToDelete.exists() && fileToDelete.isFile()) {
			log.debug("Deleting file {}", fileToDelete.getAbsoluteFile());
			fileToDelete.delete();
		}
	}

	public File[] getFiles() {
		return filesDataFolder.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(final File dir, final String name) {
				if (name.startsWith("_") || name.startsWith(".")) {
					return false;
				} else {
					return true;
				}
			}
		});
	}

	public File[] getContents() {
		val files = contentDataFolder.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(final File dir, final String name) {
				if (name.startsWith("_") || name.startsWith(".")) {
					return false;
				} else {
					if (name.endsWith(".md")) {
						return true;
					} else {
						return false;
					}
				}
			}
		});

		Arrays.sort(files);

		return files;
	}

	public String getContent(final File file) {
		String markdownText;
		try {
			@Cleanup
			final InputStreamReader inputStreamReader = new InputStreamReader(
					new FileInputStream(file), "UTF8");
			@Cleanup
			final BufferedReader in = new BufferedReader(inputStreamReader);

			final StringBuffer stringBuffer = new StringBuffer();

			String line;
			while ((line = in.readLine()) != null) {
				stringBuffer.append(line + "\n");
			}

			in.close();
			markdownText = stringBuffer.toString();
		} catch (final UnsupportedEncodingException e) {
			return error("Block encoding problem: " + e.getMessage());
		} catch (final IOException e) {
			return error("Block I/O problem: " + e.getMessage());
		} catch (final Exception e) {
			return error("Block parsing problem: " + e.getMessage());
		}

		return StringEscapeUtils.escapeHtml(markdownText);
	}

	public void setContent(final String file, final String content) {
		try {
			@Cleanup
			final FileOutputStream fileOutputStream = new FileOutputStream(
					new File(contentDataFolder, file));
			@Cleanup
			final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					fileOutputStream, "UTF8");
			@Cleanup
			final BufferedWriter out = new BufferedWriter(outputStreamWriter);

			out.write(content);
		} catch (final UnsupportedEncodingException e) {
			log.error("Error when writing block: {}", e.getMessage());
		} catch (final FileNotFoundException e) {
			log.error("Error when writing block: {}", e.getMessage());
		} catch (final IOException e) {
			log.error("Error when writing block: {}", e.getMessage());
		}
	}

	public String readableFileSize(final long size) {
		if (size <= 0) {
			return "0";
		}
		final String[] units = new String[] { "B", "KB", "MB", "GB", "TB", "EB" };
		final int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size
				/ Math.pow(1024, digitGroups))
				+ " " + units[digitGroups];
	}

	private String error(final String message) {
		return "<p class=\"error\">" + StringEscapeUtils.escapeHtml(message)
				+ "</p>";
	}
}
