package ch.ethz.mc.root.website;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import lombok.Cleanup;
import lombok.val;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang.StringEscapeUtils;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

import ch.ethz.mc.root.Constants;

/**
 * Manages Markdown conversion to HTML
 * 
 * @author Andreas Filler
 */
@Log4j2
public class MarkdownManager {
	private final File					contentDataFolder;

	private final PegDownProcessor		processor;
	private final SpecialLinkRenderer	specialLinkRenderer;

	public MarkdownManager() throws Exception {
		contentDataFolder = new File(Constants.getWebsiteDataContentPath());
		if (!contentDataFolder.isDirectory() || !contentDataFolder.exists()) {
			throw new Exception("Content data folder \""
					+ Constants.getWebsiteDataContentPath()
					+ "\" does not exist!");
		}
		processor = new PegDownProcessor(Extensions.ABBREVIATIONS
				| Extensions.AUTOLINKS | Extensions.DEFINITIONS
				| Extensions.FENCED_CODE_BLOCKS | Extensions.QUOTES
				| Extensions.SMARTS | Extensions.SMARTYPANTS
				| Extensions.STRIKETHROUGH | Extensions.TABLES
				| Extensions.WIKILINKS);
		specialLinkRenderer = new SpecialLinkRenderer();
	}

	public String getWebsite(final ServletContext context, final String resource) {
		String websiteText;
		try {
			@Cleanup
			final InputStreamReader inputStreamReader = new InputStreamReader(
					context.getResourceAsStream("/" + Constants.ROOT_REDIRECT
							+ "/template.htm"), "UTF8");
			@Cleanup
			final BufferedReader in = new BufferedReader(inputStreamReader);

			final StringBuffer stringBuffer = new StringBuffer();

			String line;
			while ((line = in.readLine()) != null) {
				stringBuffer.append(line + "\n");
			}

			in.close();
			websiteText = stringBuffer.toString();
		} catch (final UnsupportedEncodingException e) {
			return error("Website encoding problem: " + e.getMessage());
		} catch (final IOException e) {
			return error("Website I/O problem: " + e.getMessage());
		} catch (final Exception e) {
			return error("Website parsing problem: " + e.getMessage());
		}

		// Replace HTML blocks
		final Pattern blockPattern = Pattern
				.compile("\\#\\#([A-Za-z0-9_]+)\\#\\#");
		Matcher blockMatcher = blockPattern.matcher(websiteText);

		while (blockMatcher.find()) {
			final String blockHTML = getHTML(blockMatcher.group(1));
			websiteText = websiteText.substring(0, blockMatcher.start())
					+ blockHTML + websiteText.substring(blockMatcher.end());
			blockMatcher = blockPattern.matcher(websiteText);
		}

		// Replace STATISTICS blocks
		final Properties statistics = new Properties();
		try {
			@Cleanup
			final FileReader fileReader = new FileReader(new File(
					Constants.getStatisticsFile()));

			statistics.load(fileReader);
		} catch (final Exception e) {
			log.warn("Could not parse statistics file: {}", e.getMessage());

			return websiteText;
		}

		final Pattern statisticsPattern = Pattern
				.compile("\\$\\$([A-Za-z0-9_\\.]+)\\$\\$");
		Matcher statisticsMatcher = statisticsPattern.matcher(websiteText);

		while (statisticsMatcher.find()) {
			final String statisticsText = statistics
					.getProperty(statisticsMatcher.group(1));

			if (statisticsText == null) {
				continue;
			}

			websiteText = websiteText.substring(0, statisticsMatcher.start())
					+ statisticsText
					+ websiteText.substring(statisticsMatcher.end());
			statisticsMatcher = statisticsPattern.matcher(websiteText);
		}

		return websiteText;
	}

	public String getHTML(final String block) {
		val file = new File(contentDataFolder, block + ".md");

		if (!file.isFile() || !file.exists()) {
			return error("Block \"" + block + "\" does not exists!");
		}

		String markdownText;
		try {
			@Cleanup
			final InputStreamReader inputStreamReader = new InputStreamReader(
					new FileInputStream(file), "UTF8");
			@Cleanup
			final BufferedReader in = new BufferedReader(inputStreamReader);

			final StringBuffer stringBuffer = new StringBuffer();

			String line;
			while ((line = in.readLine()) != null) {
				stringBuffer.append(line + "\r");
			}

			in.close();
			markdownText = stringBuffer.toString();
		} catch (final UnsupportedEncodingException e) {
			return error("Block encoding problem: " + e.getMessage());
		} catch (final IOException e) {
			return error("Block I/O problem: " + e.getMessage());
		} catch (final Exception e) {
			return error("Block parsing problem: " + e.getMessage());
		}

		return processor.markdownToHtml(markdownText, specialLinkRenderer);
	}

	private String error(final String message) {
		return "<p class=\"error\">" + StringEscapeUtils.escapeHtml(message)
				+ "</p>";
	}
}
