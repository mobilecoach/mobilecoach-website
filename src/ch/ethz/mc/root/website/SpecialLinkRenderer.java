package ch.ethz.mc.root.website;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import static org.pegdown.FastEncoder.encode;
import static org.pegdown.FastEncoder.obfuscate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import lombok.Synchronized;

import org.parboiled.common.StringUtils;
import org.pegdown.LinkRenderer;
import org.pegdown.ast.AutoLinkNode;
import org.pegdown.ast.ExpImageNode;
import org.pegdown.ast.ExpLinkNode;
import org.pegdown.ast.MailLinkNode;
import org.pegdown.ast.RefImageNode;
import org.pegdown.ast.RefLinkNode;
import org.pegdown.ast.WikiLinkNode;

import ch.ethz.mc.root.Constants;

/**
 * Link renderer who adapts to the specific needs of internal file linking
 * 
 * @author Andreas Filler
 */
public class SpecialLinkRenderer extends LinkRenderer {

	/**
	 * Simple model class for holding the `href`, link text as well as other tag
	 * attributes of an HTML link.
	 */
	public static class SpecialRendering extends Rendering {
		public SpecialRendering(final String href, final String text) {
			super(href, text);
			if (this.href.startsWith("home:")) {
				this.href = Constants.getWebsiteDataURL() + "/"
						+ this.href.substring(5);
			}
		}
	}

	@Override
	@Synchronized
	public Rendering render(final AutoLinkNode node) {
		return new SpecialRendering(node.getText(), node.getText());
	}

	@Override
	@Synchronized
	public Rendering render(final ExpLinkNode node, final String text) {
		final Rendering rendering = new SpecialRendering(node.url, text);
		return StringUtils.isEmpty(node.title) ? rendering : rendering
				.withAttribute("title", encode(node.title));
	}

	@Override
	@Synchronized
	public Rendering render(final ExpImageNode node, final String text) {
		final Rendering rendering = new SpecialRendering(node.url, text);
		return StringUtils.isEmpty(node.title) ? rendering : rendering
				.withAttribute("title", encode(node.title));
	}

	@Override
	@Synchronized
	public Rendering render(final MailLinkNode node) {
		final String obfuscated = obfuscate(node.getText());
		return new SpecialRendering("mailto:" + obfuscated, obfuscated);
	}

	@Override
	@Synchronized
	public Rendering render(final RefLinkNode node, final String url,
			final String title, final String text) {
		final Rendering rendering = new SpecialRendering(url, text);
		return StringUtils.isEmpty(title) ? rendering : rendering
				.withAttribute("title", encode(title));
	}

	@Override
	@Synchronized
	public Rendering render(final RefImageNode node, final String url,
			final String title, final String alt) {
		final Rendering rendering = new SpecialRendering(url, alt);
		return StringUtils.isEmpty(title) ? rendering : rendering
				.withAttribute("title", encode(title));
	}

	@Override
	@Synchronized
	public Rendering render(final WikiLinkNode node) {
		try {
			final String url = "./"
					+ URLEncoder.encode(node.getText().replace(' ', '-'),
							"UTF-8") + ".html";
			return new SpecialRendering(url, node.getText());
		} catch (final UnsupportedEncodingException e) {
			throw new IllegalStateException();
		}
	}
}
