package ch.ethz.mc.root;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import lombok.val;

/**
 * Sets logging folder as folder for Log4j2 log files and adjusts configuration
 * based on configuration file
 *
 * @author Andreas Filler
 */
public class FirstInitializations implements ServletContextListener {
	@Override
	public void contextInitialized(final ServletContextEvent event) {
		String configurationsFileString = null;

		// Tries to get specific configuration based on virtual server
		// configuration
		try {
			val serverNameElements = event.getServletContext()
					.getVirtualServerName().split("/");

			configurationsFileString = System
					.getProperty(serverNameElements[serverNameElements.length - 1]
							.toLowerCase()
							+ "."
							+ Constants.SYSTEM_CONFIGURATION_PROPERTY);
		} catch (final Exception e) {
			System.err
					.println("Error at getting virtual server based configuration file: "
							+ e.getMessage());
		}

		if (configurationsFileString == null
				|| !new File(configurationsFileString).exists()) {
			configurationsFileString = System
					.getProperty(Constants.SYSTEM_CONFIGURATION_PROPERTY);
		}

		val thread = Thread.currentThread();
		thread.setName(Constants.LOGGING_APPLICATION_NAME);

		Constants.injectConfiguration(configurationsFileString,
				event.getServletContext());

		// Initialize redirect manager
		RedirectManager.init();
	}

	@Override
	public void contextDestroyed(final ServletContextEvent event) {
		// nothing to do
	}
}
