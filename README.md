# Welcome to the *MobileCoach*!

*MobileCoach* is the Open Source Behavioral Intervention Platform designed by **ETH Zurich**, the **University of St. Gallen** and the **Swiss Research Institute for Public Health and Addiction**.

© 2013-2017 [Center for Digital Health Interventions, Health-IS Lab](http://www.c4dhi.org) a joint initiative of the [Institute of Technology Management](http://www.item.unisg.ch) at [University of St. Gallen](http://www.unisg.ch) and the [Department of Management, Technology and Economics](http://mtec.ethz.ch) at [ETH Zurich](http://www.ethz.ch).   

For further information visit the *MobileCoach* Website at [https://www.mobile-coach.eu](https://www.mobile-coach.eu)!

## Team of the release version

### Center for Digital Health Interventions, Health-IS Lab

* **Andreas Filler** - afiller (AT) ethz (DOT) ch
* **Dr. Tobias Kowatsch** - tobias (DOT) kowatsch (AT) unisg (DOT) ch
* **Jost Schweinfurther** - jschweinfurther (AT) ethz (DOT) ch
* **Prof. Dr. Elgar Fleisch** - efleisch (AT) ethz (DOT) ch

### Swiss Research Institute for Public Health and Addiction

* **Dr. Severin Haug** - severin (DOT) haug (AT) isgf (DOT) uzh (DOT) ch
* **Raquel Paz Castro** - raquel (DOT) paz (AT) isgf (DOT) uzh (DOT) ch

## License

License information can be found in the [LICENSE.txt](LICENSE.txt) file in the root folder of this project.

---

# *MobileCoach Website* usage information

The *MobileCoach Website* (MCW) component offers two functionalities:

1. It enables an administrator to provide several different subdomains for specific websites for specific audiences on the server running the main **MobileCoach** system.
2. It enables an administrator to provides short URLs (also using subdomains) for redirects, e.g. to media objects.

An example website is provided in the release package in the **WEB-INF/www** folder.

The whole **MobileCoach Website** can be configured using one configuration file **web_configuration.properties** which is included in the repository containing example values. 


**Suggestion:** Use the provided **build.xml** within **Eclipse** to create the **.war** file.

### Development environment

* Java (8 or newer) SDK 
* Web application server (Apache Tomcat 8 or newer/compatible)
* Web application server configured to listen on port 80 (HTTP) and 443 (HTTPS) with a valid or self-signed certificate
* [Eclipse IDE for Java EE Developers](https://eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2) (Project files included in repository)
* [Project Lombok](http://projectlombok.org) (Can be installed using a double-click on the **.jar** file)

### Server environment

* Java (8 or newer) SDK 
* Web application server (Apache Tomcat 8 or newer/compatible)
* Web application server configured to listen on port 80 (HTTP) and 443 (HTTPS) with a valid certificate

## Basic configuration

The whole **MobileCoach Website** can be configured using one configuration file **web_configuration.properties** which is included in the repository containing example values. All folders mentioned in this file should be created before the first startup of the **MobileCoach Website** system.

Your adjusted copy of the **web_configuration.properties** can be placed anywhere on your system, but the file need to be referenced as system property with the name **mc_website.configuration** (or **virtualservername.xyz.mc_website.configuration** if you run several virtual servers). Otherwise the default configuration from the **Constants.java** will be used - and you won't like it.

To define the system property you can start your java runtime with the following parameter:
	
	-Dmc_website.configuration="/PATH_TO_YOUR_FILE/web_configuration.properties"

Extend your tomcat configuration files or startup scripts with this setting.

If you are using Tomcat8.5 or newer, with the support for virtual servers, then you can provide several configuration files for each server by adjusting the configuration parameter as follows:

	-Dvirtualservername.xyz.mc_website.configuration="/PATH_TO_YOUR_FILE/web_configuration.properties"

Also the following should be added to avoid encoding problems on several operating systems and/or RAM problems:

	-server -Dfile.encoding=UTF8 -Djava.awt.headless=true -Xms2g -Xmx2g

**Suggestion:**   
Ensure that your Tomcat installation is allowed to read and write to all folders configured in the **configuration.properties**

**Suggestion:**   
*Mobile Coach* is tested with Apache Tomcat 8(.5)

## Redirect configuration

The mapping between the subdomains and the web folders can be done in the **src/ch.ethz.mc.root/RedirectManager.java** enumeration **AllowedSubDomains**. All redirects containing hash symbols (#) and the WWW element should not be deleted, while the other subcomains can be configured are required for the appropriate server setup, e.g. 

	DEV("developer")

configures the server to provide the website available in **WebContent/WEB-INF/www/developer** on the subdomain **dev.YOURDOMAIN.WHATEVER**

## Mini CMS configuration

Calling the root URL of the website leads to a specific behaviour instead of all other calls: It redirects the user to the main website of the system. This website is based on the file **WebContent/WEB-INF/www/root/template.htm**. This file can contain several placeholders in the form **##NUMBERS_OR_LETTERS##** as CMS placeholders or **$$PROPERTY_NAME_FROM_STATISTICS_FILE$$**.

This website can be managed using the URL **/admin** on your root URL to access the CMS backend. A tomcat user with the user role **mobile-coach** has to be configured in the web container to access this website.

All placeholers used in the template need to be created as empty files in the **WEBSITE_DATA_CONTENT_PATH** configured in the **Constants.java** with the extension **.md**.

For example:

	##01_Introduction##
	
requires a file **01_Introduction.md** to be created in the **WEBSITE_DATA_CONTENT_PATH**.

The file **WebContent/WEB-INF/www/root/_instructions.md** should also be moved to the **WEBSITE_DATA_CONTENT_PATH** to provide basic usage information in the CMS backend.

As you can see in the instructions file the CMS uses a very simple **Markdown** syntax.

An example **template.htm** is already provided in the release.

## Further information

For further information have a look at the **README.md** file in the main repository of the *MobileCoach*.


### Version 1.1.8 (Build 20170629)