Headlines
---

```
Main headline
===

Sub headline
---
```

or

````
# Main headline

## Sub headline
````

Lists
---

```
1. First item
2. Second item
3. Third item

* First item
* Second item
* Third item
```

Text styles
---

```
*italic*   **bold**   
```
or

```
_italic_   __bold__
```

Block styles
---

```
 ```
 Fenced code blocks are preformatted blocks
 with linebreak-support but without indents
 ```
```

````
   Preformatted blocks with indents can also simply be made
   with three or more spaces in the beginning
````

````
> Email-style angle brackets
> are used for blockquotes.
````

Regular text can be simply written in the file. If you need a line-break without a a new paragraph simply write three or more spaces at the end of the line.   
Before you start a new line.

Linking
---

````
In every text [direct links][http://mypage.com] can be used. But it's much nices to create [references][url1] and set them at the end of the text.

[url1]: http://example.com/  "Title"
````

````
Anchors can be used to [jump](#anchor1) inside the document from, e.g. a header:

## [Header with anchor](#anchor1)
````

````
Images and files can be linked the same way:

![Alternative Text](home:THEFILENAMEHERE.PNG)

or

![Alternative Text][image1]

[File Description](home:THEFILENAMEHERE.ZIP)

or

[File Description][file1]

[image1]: home:THEFILENAMEHERE.PNG "Title of the image"
[file1]: home:THEFILENAMEHERE.ZIP
````

````
Email links are possible as well this way:

[Martin Miller](mailto:example@example.com)

or

Martin Miller <example@example.com>
````

## Tables

Tables can be realized in several ways and with different alignments as it can be seen in the following:

````
First Header | Second Header | Third Header
------------ | :-----------: | -----------:
Left         | Center        | Right
Left         | Center        | Right


| First Header | Second Header | Third Header |
| ------------ | ------------- | ------------ |
| Content Cell | Content Cell  | Content Cell |
| Content Cell | Content Cell  | Content Cell |


First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell
````

You can also play with other attributes:

````
With header:

First | Second
------|-------
Cool  | Table
[this is the caption]


Without header:

|--------|-------|
|Cool    | Table |
|I like  | it    |


With some alignment:

:-----|-----:|----|:---:
Cool  | Info | in | here


And now to some colspan:

Names ||
 Name | Firstname | Age
------|-----------|----:
  Fox | Peter     | 42
  Guy | Ritchie   | 60

````

A complex example can be:

````
|             |          Grouping           ||
First Header  | Second Header | Third Header |
 ------------ | :-----------: | -----------: |
Content       |          *Long Cell*        ||
Content       |   **Cell**    |         Cell |
New section   |     More      |         Data |
And more      |               |   And more   |
[this is the caption]

````