<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="ch.ethz.mc.root.website.MarkdownManager"%>
<%
	/*
	 * Copyright (C) 2013-2015 MobileCoach Team at the Health-IS Lab
	 * 
	 * For details see README.md file in the root folder of this project.
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 */

	MarkdownManager mm = new MarkdownManager();
	/*
	if (request.getSession().getAttribute("AUTHORIZED") != null
			&& (Boolean) request.getSession()
					.getAttribute("AUTHORIZED")) {
	 */
%>
<%=mm.getWebsite(getServletContext(),
					"www/root/template.htm")%>
<%
	/*
	} else {
		response.getWriter().print("Under construction!");
	}
	 */
%>

