<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="ch.ethz.mc.root.Constants"%>
<%@page import="java.io.File"%>
<%@page import="ch.ethz.mc.root.website.WebsiteAdministrationManager"%>
<%@page import="ch.ethz.mc.root.website.MarkdownManager"%>
<%
	/*
	 * Copyright (C) 2013-2015 MobileCoach Team at the Health-IS Lab
	 * 
	 * For details see README.md file in the root folder of this project.
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 */

	MarkdownManager mm = new MarkdownManager();
	WebsiteAdministrationManager wam = new WebsiteAdministrationManager(
			request);

	//request.getSession().setAttribute("AUTHORIZED", true);
%>
<html>
<head>
<title>MobileCoach Website Administration</title>
<link rel="stylesheet" type="text/css" href="admin.css" />
</head>
<body>
	<h1 class="italic">MobileCoach Website Administration</h1>
	<small>Version <%=Constants.getVersion()%></small>
	<h1>Files</h1>
	<%
		wam.proceedUpload();

		if (wam.isAction("delete")) {
			wam.deleteFile(request.getParameter("file"));
		} else if (wam.isAction("update")) {
			wam.setContent(request.getParameter("file"),
					request.getParameter("content"));
		}
	%>
	<h2>Manage Files</h2>
	<table>
		<tr>
			<th>Name</th>
			<th>Size</th>
			<th>Action</th>
			<%
				File[] files = wam.getFiles();
				for (File file : files) {
			%>
		
		<tr>
			<td><a
				href="<%=Constants.getWebsiteDataURL() + "/" + file.getName()%>"
				target="_blank"><%=file.getName()%></a></td>
			<td><%=wam.readableFileSize(file.length())%></td>
			<td><form method="post" action="#">
					<input type="hidden" name="action" value="delete" /><input
						type="hidden" name="file" value="<%=file.getName()%>" /><input
						type="submit" value="Delete" />
				</form></td>
		</tr>
		<%
			}

			if (files.length == 0) {
		%>
		<tr>
			<td colspan="3">no files available</td>
		</tr>
		<%
			}
		%>
	</table>
	<h2>Upload Files</h2>
	<form method="post" enctype="multipart/form-data" action="#">
		<input type="hidden" name="action" value="upload" /> <input
			type="file" name="file" /><input type="submit" value="Upload" />
	</form>
	<h1>Content</h1>
	<%
		int i = 0;
		for (File file : wam.getContents()) {
			i++;
	%>
	<a href="#anchor_<%=i%>_top"><%=file.getName()
						.substring(0, file.getName().length() - 3)
						.replaceAll("_", " ")%></a>
	<br />
	<%
		}
		i = 0;
		for (File file : wam.getContents()) {
			i++;
	%>
	<h2 id="anchor_<%=i%>_top"><%=file.getName()
						.substring(0, file.getName().length() - 3)
						.replaceAll("_", " ")%></h2>
	<h3>Current Version</h3>
	<div class="content">
		<%=mm.getHTML(file.getName().substring(0,
						file.getName().length() - 3))%>
	</div>
	<h3 id="anchor_<%=i%>">Edit</h3>
	<form method="post" action="#anchor_<%=i%>">
		<input type="hidden" name="action" value="update" /><input
			type="hidden" name="file" value="<%=file.getName()%>" />
		<textarea rows="20" cols="80" name="content"><%=wam.getContent(file)%></textarea>
		<br /> <input type="submit" value="Save" />
	</form>
	<%
		}
	%>

	<h1>Instructions</h1>
	<%=mm.getHTML("_instructions")%>
</body>
</html>
